package amandaeller.madcourse.neu.edu.slowloris;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Amanda on 4/4/2017.
 */

public class AccelerometerActivity extends AppCompatActivity implements SensorEventListener {
  public SharedPreferences sharedPref;
  public SharedPreferences sharedPrefScore;

  public SensorManager sensorManager;
  public Sensor sensor;
  public final float[] accelMatrix = new float[3];
  public AccelView accelView;
  public static int tic = UC.TIC;

  protected String vibratorService = Context.VIBRATOR_SERVICE;
  protected static Vibrator vibrator;
  private static SoundPool soundPool;
  private static int alertSound;
  private static int passSound;
  private static int missSound;
  private boolean muteSounds;

  @Override
  public void onCreate(Bundle savedInstaceState){
    super.onCreate(savedInstaceState);
    setContentView(R.layout.activity_accelerometer);

    sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    setUpSharedPref();

    accelMatrix[0] = 0;
    accelMatrix[1] = 0;
    accelMatrix[2] = 0;

    accelView = (AccelView) findViewById(R.id.sensor_accel_view);

    vibrator = (Vibrator)getSystemService(vibratorService);
    soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
    alertSound = soundPool.load(this, R.raw.gentle_alarm, 1);
    passSound = soundPool.load(this, R.raw.here_it_is, 3);
    missSound = soundPool.load(this, R.raw.hiccup, 2);
  }

  private void setUpSharedPref(){
    sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
    sharedPrefScore = this.getSharedPreferences(UC.PROFILE_KEY, Context.MODE_PRIVATE);
    muteSounds = sharedPref.getBoolean(UC.KEY_MUTE_SOUNDS, false);
  }

  public SharedPreferences getSharedPref(){
    sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
    return sharedPref;
  }

  @Override
  public void onSensorChanged(SensorEvent event) {
    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
      accelMatrix[0] = event.values[0];
      accelMatrix[1] = event.values[1];
      accelMatrix[2] = event.values[2];

      accelView.setAccelMatrix(accelMatrix);
    }
  }

  @Override
  public void onPause(){
    super.onPause();
    sensorManager.unregisterListener(this);
  }

  @Override
  public void onResume(){
    super.onResume();
    sensorManager.registerListener(this, sensor, tic);
  }

  public void missedTarget() {
    sensorManager.unregisterListener(this);
    if(!muteSounds){soundPool.play(missSound, 1f,1f,1,0,1f);}
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(R.string.missedTargetMsg)
        .setTitle(R.string.missedTargetTitle)
        .setPositiveButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            accelView.resetScreen();
            accelView.resetTrace();
            sensorManager.registerListener(AccelerometerActivity.this, sensor, tic);
          }
        })
        .setNegativeButton(R.string.mainMenu, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        });
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  protected void completedPass(){
    sensorManager.unregisterListener(this);
    if(!muteSounds){soundPool.play(passSound, 1f,1f,1,0,1f);}
    updateScore();
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(R.string.completedPassMsg)
        .setTitle(R.string.completedPassTitle)
        .setPositiveButton(R.string.playAgain, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            accelView.resetScreen();
            accelView.resetTrace();
            sensorManager.registerListener(AccelerometerActivity.this, sensor, tic);
          }
        })
        .setNegativeButton(R.string.mainMenu, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        });
    AlertDialog dialog = builder.create();
    dialog.show();
  }

  private void updateScore() {
    SharedPreferences.Editor editor = sharedPrefScore.edit();
    int defaultvalue = UC.DEFAULT_SCORE;
    int total = sharedPrefScore.getInt(UC.KEY_BOTH_TOTAL_PASS, defaultvalue);
    int classTotal, lowTotal, midTotal, highTotal;

    String sensLevel = sharedPref.getString(UC.KEY_SENSITIVITY_LEVEL, null);

    if(accelView.targetPlay){
      classTotal = sharedPrefScore.getInt(UC.KEY_TARGET_TOTAL_PASS, defaultvalue);
      lowTotal = sharedPrefScore.getInt(UC.KEY_TARGET_LOW_TOTAL, defaultvalue);
      midTotal = sharedPrefScore.getInt(UC.KEY_TARGET_MID_TOTAL, defaultvalue);
      highTotal = sharedPrefScore.getInt(UC.KEY_TARGET_HIGH_TOTAL, defaultvalue);
    }else {
      classTotal = sharedPrefScore.getInt(UC.KEY_TOTAL_PASS, defaultvalue);
      lowTotal = sharedPrefScore.getInt(UC.KEY_LOW_TOTAL, defaultvalue);
      midTotal = sharedPrefScore.getInt(UC.KEY_MID_TOTAL, defaultvalue);
      highTotal = sharedPrefScore.getInt(UC.KEY_HIGH_TOTAL, defaultvalue);
    }

    if(sensLevel.equals("70")){
      lowTotal++;
      if(accelView.targetPlay && accelView.isTargetAlertOn){
        editor.putInt(UC.KEY_TARGET_LOW_TOTAL, lowTotal);
      }else{
        editor.putInt(UC.KEY_LOW_TOTAL, lowTotal);
      }
    }

    if(sensLevel.equals("50")){
      midTotal++;
      if(accelView.targetPlay && accelView.isTargetAlertOn){
        editor.putInt(UC.KEY_TARGET_MID_TOTAL, midTotal);
      }else {
        editor.putInt(UC.KEY_MID_TOTAL, midTotal);
      }
    }

    if(sensLevel.equals("40")){
      highTotal++;
      if(accelView.targetPlay && accelView.isTargetAlertOn){
        editor.putInt(UC.KEY_TARGET_HIGH_TOTAL, highTotal);
      }else {
        editor.putInt(UC.KEY_HIGH_TOTAL, highTotal);
      }
    }

    classTotal++;
    if(accelView.targetPlay){
      editor.putInt(UC.KEY_TARGET_TOTAL_PASS, classTotal);
    }else{
      editor.putInt(UC.KEY_TOTAL_PASS, classTotal);
    }

    total++;
    editor.putInt(UC.KEY_BOTH_TOTAL_PASS, total);
    editor.commit();
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {}

  public void vibrate() {vibrator.vibrate(250);} // Vibrate for 0.25 second.

  public void playAudioAlert() {soundPool.play(alertSound, 1f,1f,1,0,1f);}

  @Override
  public void finish() {
    super.finish();
    AccelView.targetPlay = false;
  }
}
