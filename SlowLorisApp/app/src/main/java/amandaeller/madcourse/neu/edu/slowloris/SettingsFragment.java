package amandaeller.madcourse.neu.edu.slowloris;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Amanda on 4/5/2017.
 */

public class SettingsFragment extends PreferenceFragment {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Load the preferences from an XML resource
    addPreferencesFromResource(R.xml.preferences);
  }
}
