package amandaeller.madcourse.neu.edu.slowloris;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Amanda on 4/3/2017.
 */

public class SettingsActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
     // Display the fragment as the main content.
    getFragmentManager().beginTransaction()
        .replace(android.R.id.content, new SettingsFragment())
        .commit();
  }
}
