package amandaeller.madcourse.neu.edu.slowloris;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import java.util.Random;

/**
 * Based off the code that Professor Slaughter used in lecture 8
 */

public class AccelView extends View {
  private SharedPreferences settings;
  private float threshold;
  private boolean isAudioOn;
  private boolean isVibrateOn;
  public boolean isTargetAlertOn;
  public static boolean targetPlay = false;

  private int time = 0;
  private int resetAlertTimer = 50;
  private float[] accelMatrix = new float[3];
  private Trace trace = new Trace();
  private AccelerometerActivity app;

  private Bitmap myBitmap;
  private Canvas myCanvas;
  private Paint xPaint = new Paint();
  private Paint yPaint = new Paint();
  private Paint zPaint = new Paint();
  private Paint wPaint = new Paint();
  private Paint bgPaint = new Paint();

  public AccelView(Context context) {
    super(context);
    createBuffer();
    init();
  }

  public AccelView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupPaint();
    ViewTreeObserver viewTreeObserver = getViewTreeObserver();
    if (viewTreeObserver.isAlive()) {
      viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
          getViewTreeObserver().removeOnGlobalLayoutListener(this);
          createBuffer();
        }
      });
    }
    createBuffer();
    init();
  }

  private void init() {
    app = (AccelerometerActivity) getContext();
    settings = app.getSharedPref();
    threshold = Float.parseFloat(settings.getString(UC.KEY_SENSITIVITY_LEVEL,"80"));
    isAudioOn = settings.getBoolean(UC.KEY_AUDIO_ALERT, false);
    isVibrateOn = settings.getBoolean(UC.KEY_VIBRATE_ALERT, true);
    isTargetAlertOn = settings.getBoolean(UC.KEY_TARGET_ALERT, false);
    if(targetPlay && isTargetAlertOn){
      threshold = threshold + 35; //Target play is already challenging, add a little to make it with motion alerts a bit easier
    }
  }

  private void setupPaint() {
    xPaint.setColor(Color.RED);
    xPaint.setStrokeWidth(UC.STROKE_WIDTH);
    xPaint.setTextSize(UC.TEXT_SIZE);
    xPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    yPaint.setColor(Color.CYAN);
    yPaint.setStrokeWidth(UC.STROKE_WIDTH);
    yPaint.setTextSize(UC.TEXT_SIZE);
    yPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    zPaint.setColor(Color.GREEN);
    zPaint.setStrokeWidth(UC.STROKE_WIDTH);
    zPaint.setTextSize(UC.TEXT_SIZE);
    zPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    wPaint.setColor(Color.WHITE);
    wPaint.setStrokeWidth(UC.STROKE_WIDTH);
    wPaint.setTextSize(UC.TEXT_SIZE);
    wPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    bgPaint.setColor(Color.argb(255, 50, 50, 50));
  }

  public void setAccelMatrix(float[] newVals) {
    time++;
    resetAlertTimer++;
    int w = getWidth();
    if (w> 0 && time > w) { //w>0 fixes bug when screen width isn't set up yet and this passes letting completed pass dialog appear and score updated
      app.completedPass();
    }
    if (trace.isNotFull()) {
      trace.buff[trace.nBuff][0] = newVals[0];
      trace.buff[trace.nBuff][1] = newVals[1];
      trace.buff[trace.nBuff][2] = newVals[2];
      trace.nBuff++;
    }
    updateValues(myCanvas, accelMatrix.clone(), newVals.clone(), time);
    accelMatrix = newVals.clone();
    invalidate();
  }

  protected void createBuffer() {
    int w = getWidth();
    int h = getHeight();
    myBitmap = Bitmap.createBitmap(((w == 0) ? 1 : w),
        ((h == 0) ? 1 : h), Bitmap.Config.ARGB_8888);
    myCanvas = new Canvas(myBitmap);
    myCanvas.drawRect(0, 0, w, h, bgPaint);
  }

  protected void sendAlert(){
    if(resetAlertTimer > 50) {
      if (isVibrateOn) {
        app.vibrate();
      }
      if (isAudioOn) {
        app.playAudioAlert();
      }
      resetAlertTimer = 0;
    }
  }

  protected void resetScreen(){time = 0; createBuffer();}

  protected void resetTrace() {trace.nBuff = 0; trace.setupTargets(100);}

  protected void updateValues(Canvas canvas, float[] oldVals, float[] newVals, int time) {
    if(!targetPlay || (targetPlay && isTargetAlertOn)) {
      if (trace.isMovingTooFast()) {
        sendAlert();
        resetScreen();
        resetTrace();
        return;
      }
    }

    float newX = UC.X_MED + (newVals[0] * UC.SCALE); float oldX = UC.X_MED + (oldVals[0] * UC.SCALE);
    float newY = UC.Y_MED + (newVals[1] * UC.SCALE); float oldY = UC.Y_MED + (oldVals[1] * UC.SCALE);
    float newZ = UC.Z_MED + (newVals[2] * UC.SCALE); float oldZ = UC.Z_MED + (oldVals[2] * UC.SCALE);

    if (canvas != null && trace.isNotFull()) {
      canvas.drawLine(time - 1, oldX, time, newX, xPaint);
      canvas.drawLine(time - 1, oldY, time, newY, yPaint);
      canvas.drawLine(time - 1, oldZ, time, newZ, zPaint);
    }

    if (targetPlay) {
      canvas.drawCircle(trace.x, trace.y, UC.RADIUS, wPaint);

      float dx = (time - trace.x)*(time - trace.x);
      float c = UC.RADIUS*UC.RADIUS;
      float yx = (newX - trace.y)*(newX - trace.y);
      float yy = (newY - trace.y)*(newY - trace.y);
      float yz = (newZ - trace.y)*(newZ - trace.y);

      if (dx + yx < c) {
        canvas.drawCircle(trace.x, trace.y, UC.RADIUS, xPaint);
        trace.setupTargets(time + 50);
      }
      if (dx + yy < c) {
        canvas.drawCircle(trace.x, trace.y, UC.RADIUS, yPaint);
        trace.setupTargets(time + 50);
      }
      if (dx + yz < c) {
        canvas.drawCircle(trace.x, trace.y, UC.RADIUS, zPaint);
        trace.setupTargets(time + 50);
      }
      if(time > trace.x + 2 * UC.RADIUS ){
        app.missedTarget();
      }
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawBitmap(myBitmap, 0, 0, null);
  }

  public class Trace {
    private int nBuff = 0;
    private static final int N = UC.TRACE_LENGTH;
    private final float[][] buff;
    private int x;
    private int y;
    private int window = UC.WINDOW;

    protected Trace() {
      buff = new float[N][3];
      setupTargets(100);
    }

    private void setupTargets(int offset) {
      Random rnd = new Random();
      y = rnd.nextInt(1400) + 100;
      x = rnd.nextInt(200) + offset;
    }

    protected boolean isNotFull() {return nBuff < N;}

    protected boolean isMovingTooFast() {
      if (nBuff < window) {
        return false; //false = smoothing movement, haven't jerked the phone around
      }
      float aveX = 0;
      float aveY = 0;
      float aveZ = 0;
      for (int i = nBuff - window; i < nBuff; i++) { //get the average for the window right before now
        aveX = aveX + buff[i][0];
        aveY = aveY + buff[i][1];
        aveZ = aveZ + buff[i][2];
      }
      aveX = aveX/window;
      aveY = aveY/window;
      aveZ = aveZ/window;
      float bd = 0;
      for(int i = nBuff - window; i< nBuff; i++ ){ //look at the differences between the average and now
        float x = Math.abs(buff[i][0] - aveX);
        float y = Math.abs(buff[i][1] - aveY);
        float z = Math.abs(buff[i][2] - aveZ);
        if(x>bd){bd = x * 100;} //update the biggest difference
        if(y>bd){bd = y * 100;}
        if(z>bd){bd = z * 100;}
      }
      return bd>threshold; //if the larges difference is bigger then the threshold you have moved to fast
    }
  }
}
