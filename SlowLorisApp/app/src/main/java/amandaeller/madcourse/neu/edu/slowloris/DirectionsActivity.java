package amandaeller.madcourse.neu.edu.slowloris;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Amanda on 3/11/2017.
 */
public class DirectionsActivity extends Activity{
  public static boolean extraChallenge = false;

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    if(!extraChallenge) {
      setContentView(R.layout.activity_directions);
    }else {
      setContentView(R.layout.activity_challenge);
    }
  }

  @Override
  public void finish(){
    super.finish();
    extraChallenge = false;
  }
}
