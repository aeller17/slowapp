package amandaeller.madcourse.neu.edu.slowloris;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Amanda on 4/3/2017.
 */

public class MainFragment extends Fragment {
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_main, container, false);

    View startButton = rootView.findViewById(R.id.start_button);
    View targetButton = rootView.findViewById(R.id.start_dot_button);
    View challengeButton = rootView.findViewById(R.id.challenge_rec_button);
    View settingsButton = rootView.findViewById(R.id.settings_button);
    View profileButton = rootView.findViewById(R.id.profile_button);
    View directionsButton = rootView.findViewById(R.id.directions_button);
    View ackButton = rootView.findViewById(R.id.ack_button);

    startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AccelView.targetPlay = false;
        Intent intent = new Intent(getActivity(), AccelerometerActivity.class);
        getActivity().startActivity(intent);
      }
    });
    targetButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AccelView.targetPlay = true;
        Intent intent = new Intent(getActivity(), AccelerometerActivity.class);
        getActivity().startActivity(intent);
      }
    });
    challengeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        DirectionsActivity.extraChallenge = true;
        Intent intent = new Intent(getActivity(), DirectionsActivity.class);
        getActivity().startActivity(intent);
      }
    });
    settingsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        getActivity().startActivity(intent);
      }
    });
    profileButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        getActivity().startActivity(intent);
      }
    });
    directionsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), DirectionsActivity.class);
        getActivity().startActivity(intent);
      }
    });
    ackButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.acknowledgement_title);
        builder.setMessage(R.string.acknowledgement_content);
        builder.show();
      }
    });
    return rootView;
  }

}
