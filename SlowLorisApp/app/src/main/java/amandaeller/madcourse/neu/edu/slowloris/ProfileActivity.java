package amandaeller.madcourse.neu.edu.slowloris;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Amanda on 4/3/2017.
 */

public class ProfileActivity extends Activity {
  public SharedPreferences sharedPreferences;
  private int defaultValue;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    defaultValue = UC.DEFAULT_SCORE;
    sharedPreferences = this.getSharedPreferences(UC.PROFILE_KEY, Context.MODE_PRIVATE);

    int classTotal = sharedPreferences.getInt(UC.KEY_TOTAL_PASS, defaultValue);
    int lowTotal = sharedPreferences.getInt(UC.KEY_LOW_TOTAL, defaultValue);
    int midTotal = sharedPreferences.getInt(UC.KEY_MID_TOTAL, defaultValue);
    int highTotal = sharedPreferences.getInt(UC.KEY_HIGH_TOTAL, defaultValue);

    TextView ctot = (TextView)findViewById(R.id.CompletedPassesNum);
    ctot.setText("" + classTotal);

    TextView low = (TextView)findViewById(R.id.LowPassesNum);
    low.setText("" + lowTotal);

    TextView mid = (TextView)findViewById(R.id.midPassesNum);
    mid.setText("" + midTotal);

    TextView high = (TextView)findViewById(R.id.highPassesNum);
    high.setText("" + highTotal);

    int targetTotal = sharedPreferences.getInt(UC.KEY_TARGET_TOTAL_PASS, defaultValue);
    int tLowTotal = sharedPreferences.getInt(UC.KEY_TARGET_LOW_TOTAL, defaultValue);
    int tMidTotal = sharedPreferences.getInt(UC.KEY_TARGET_MID_TOTAL, defaultValue);
    int tHighTotal = sharedPreferences.getInt(UC.KEY_TARGET_HIGH_TOTAL, defaultValue);

    TextView tTotal = (TextView)findViewById(R.id.TargetCompletedPassesNum);
    tTotal.setText("" + targetTotal);

    TextView tLow = (TextView)findViewById(R.id.TargetLowPassesNum);
    tLow.setText("" + tLowTotal);

    TextView tMid = (TextView)findViewById(R.id.TargetMidPassesNum);
    tMid.setText("" + tMidTotal);

    TextView tHigh = (TextView)findViewById(R.id.TargetHighPassesNum);
    tHigh.setText("" + tHighTotal);

    int bothPass = sharedPreferences.getInt(UC.KEY_BOTH_TOTAL_PASS, defaultValue);

    TextView bothTotal = (TextView)findViewById(R.id.BothTotalPlayNum);
    bothTotal.setText("" + bothPass);
  }
}
