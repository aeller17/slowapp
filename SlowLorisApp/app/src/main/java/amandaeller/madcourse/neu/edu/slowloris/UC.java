package amandaeller.madcourse.neu.edu.slowloris;

/**
 * Created by Amanda on 4/7/2017.
 * UC stands for Universal Constants
 */

public class UC {
  public static final int X_MED = 400,
                          Y_MED = 800,
                          Z_MED = 1200,
                          SCALE = 30,
                          WINDOW = 8,
                          TRACE_LENGTH = 1500,
                          TIC = 50000,
                          DEFAULT_SCORE = 0;

  public static final float STROKE_WIDTH = 4.0f,
                            TEXT_SIZE = 28.0f,
                            RADIUS = 30;

  public static final String KEY_AUDIO_ALERT = "audio_switch_key",
                             KEY_VIBRATE_ALERT = "vibrate_switch_key",
                             KEY_SENSITIVITY_LEVEL = "sensitivity_level_list",
                             KEY_TARGET_ALERT = "target_speed_key",
                             KEY_MUTE_SOUNDS = "mute_sounds_key",

                             PROFILE_KEY = "profile_key",
                             KEY_BOTH_TOTAL_PASS = "both_total_score",

                             KEY_TOTAL_PASS = "total_score",
                             KEY_LOW_TOTAL = "total_low_score",
                             KEY_MID_TOTAL = "total_mid_score",
                             KEY_HIGH_TOTAL = "total_high_score",

                             KEY_TARGET_TOTAL_PASS = "target_total_score",
                             KEY_TARGET_LOW_TOTAL = "target_total_low_score",
                             KEY_TARGET_MID_TOTAL = "target_total_mid_score",
                             KEY_TARGET_HIGH_TOTAL = "target_total_high_score";
}
